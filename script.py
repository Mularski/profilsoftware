import argparse
import json
import os

import requests

from db import Base, session, Location, Login, Person, Timezone, engine


def add_args_to_parser(parser):
    parser.add_argument('--gender-percentage', help="Show percentage of people by gender.")
    parser.add_argument('--most-common-passwords', type=int, help="Show N most popular passwords.")
    parser.add_argument('--most-common-cities', type=int, help="Show N most popular cities.")
    parser.add_argument('--born-between', nargs=2, help="show information about people who were born between two passed dates. Format: YYYY-MM-DD, dates should be separated with a spacebar.")
    parser.add_argument('--average-age', choices=['all', 'male', 'female'], help="Show average age of either male, female population or both.")
    parser.add_argument('--most-secure-password', help="Show average age of either male, female population or both.")
    parser.add_argument('--use-api', type=int, help="Load data to the database from API instead of file. Provide the number of desired database entries.")

def load_data(use_api: bool):
    data = ''

    if use_api:
        address = 'https://randomuser.me/api/?results=' + str(use_api)
        request = requests.get(address)
        data = request.text
    else:
        file = open('persons.json', 'r+')
        data = file.read()

    return json.loads(data)['results']

def save_data(data):
    for result in data:
        login = Login(**result['login'])
        timezone = Timezone(**result['location']['timezone'])
        session.add_all([login, timezone])
        session.commit()
        
        location_info = result['location']
        location_info.pop('timezone')
        street = location_info.pop('street')
        coordinates = location_info.pop('coordinates')
        location=Location(**location_info)
        location.street_name = street['name']
        location.street_number = street['number']
        location.longitude = coordinates['longitude']
        location.latitude = coordinates['latitude']
        location.timezone_id = timezone.id
        session.add(location)
        session.commit()
        
        person = Person(
            gender=result['gender'],
            email=result['email'],
            date_of_birth=result['dob']['date'],
            age=result['dob']['age'],
            registration_date=result['registered']['date'],
            years_from_registration=result['registered']['age'],
            phone=result['phone'].replace('-', ''),
            cell=result['phone'].replace('-', ''),
            identity_name=result['id']['name'],
            identity_value=result['id']['value'],
            nationality=result['nat'],
            title=result['name']['title'],
            first_name=result['name']['first'],
            last_name=result['name']['last'],
            login_id=login.id,
            location_id=location.id
        )
        session.add(person)
        session.commit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    add_args_to_parser(parser)
    args = parser.parse_args()

    Base.metadata.create_all(engine)
    data = load_data(args.use_api)
    save_data(data)

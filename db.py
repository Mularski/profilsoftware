import os
from sqlalchemy import create_engine, ForeignKey, String, Integer, Column
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

if not os.path.exists('database.db'):
    with open('database.db', 'w'): pass
engine = create_engine('sqlite:///database.db')
conn = engine.connect()
session = sessionmaker(bind=engine)()
Base = declarative_base()

class NameMixin:
    title = Column(String)
    first_name = Column(String)
    last_name = Column(String)

class Person(Base, NameMixin):
    __tablename__ = 'person'

    id = Column(Integer, primary_key=True)
    gender = Column(String)
    email = Column(String)
    date_of_birth = Column(String)
    age = Column(String)
    registration_date = Column(String)
    years_from_registration = Column(String)
    phone = Column(String)
    cell = Column(String)
    identity_name = Column(String)
    identity_value = Column(String)
    nationality = Column(String)

    login_id = Column(Integer, ForeignKey('login.id'))
    location_id = Column(Integer, ForeignKey('location.id'))

    def __repr__(self):
        return f'{self.name} {self.surname}'

class Location(Base):
    __tablename__ = 'location'

    id = Column(Integer, primary_key=True)
    street_number = Column(Integer)
    street_name = Column(String)
    city = Column(String)
    state = Column(String)
    country = Column(String)
    postcode = Column(Integer)
    longitude = Column(String)
    latitude = Column(String)
    timezone_id = Column(Integer, ForeignKey('timezone.id'))

class Timezone(Base):
    __tablename__ = 'timezone'

    id = Column(Integer, primary_key=True)
    offset = Column(String)
    description = Column(String)

class Login(Base):
    __tablename__ = 'login'

    id = Column(Integer, primary_key=True)
    uuid = Column(String)
    username = Column(String)
    password = Column(String)
    salt = Column(String)
    md5 = Column(String)
    sha1 = Column(String)
    sha256 = Column(String)

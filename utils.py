import copy

def password_strength(password: str):
    validator_tag = 'validator'
    value_tag = 'value'
    d = {
            'small': {value_tag: 1, validator_tag: lambda x: x.lower() == x},
            'big': {value_tag: 2, validator_tag: lambda x: x.upper() == x},
            'digit': {value_tag: 1, validator_tag: lambda x: x.isdigit()},
            'special': {value_tag: 3, validator_tag: lambda x: not x.isalnum() and not x.isspace()}
        }
    sum = 0
    for letter in password:
        for key, dic in copy.copy(d).items():
            if dic[validator_tag](letter):
                sum += dic[value_tag]
                del d[key]

    sum += 8 if len(password) > 8 else 0
    return sum

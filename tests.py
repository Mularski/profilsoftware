import pytest

from utils import password_strength

def test_password_strength():
    assert password_strength('Aa') == 3
    assert password_strength('Aa1') == 4
    assert password_strength('Aa111111@11') == 15
    assert password_strength('Aa@') == 6
